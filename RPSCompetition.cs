RPSCompetition comp = new RPSCompetition();
comp.runRoundRobin();


class RPSCompetition{
    public Player[] players;
    public const string ROCK = "rock";
    public const string PAPER = "paper";
    public const string SCISSORS = "scissors";


    //Set up the teams
    public void buildTeams(){
        this.players = new Player[6];
        this.players[0] = new Player("team1", new RPSRandomAi());
        this.players[1] = new Player("team2", new RPSRandomAi());
        this.players[2] = new Player("team3", new RPSScissorAi());
        this.players[3] = new Player("team4", new RPSWrongAi());
        this.players[4] = new Player("team5", new RPSRandomAi());
        this.players[5] = new Player("team6", new RPSRandomAi());
    }

    //Run a single round of an RPS game
    public void runRPSGame(Player p1, Player p2){
        string p1choice;
        string p2choice;
        try{
            p1choice = p1.Ai.GetChoice(new Player(p1),new Player(p2));
            if (string.IsNullOrEmpty(p1choice)){
                p1choice = "null";
            }
        }catch(Exception ex){
            p1choice = "error";
        }

        try{
            p2choice = p2.Ai.GetChoice(new Player(p2),new Player(p1));
            if (string.IsNullOrEmpty(p2choice)){
                p2choice = "null";
            }
        }catch(Exception ex){
            p2choice = "error";
        }

        Console.WriteLine($"{p1.Name} chose {p1choice}, {p2.Name} chose {p2choice}");

        bool p1rock = p1choice == ROCK;
        bool p1paper = p1choice == PAPER;
        bool p1scissors = p1choice == SCISSORS;
        bool p1error = !p1rock&&!p1paper&&!p1scissors;

        bool p2rock = p2choice == ROCK;
        bool p2paper = p2choice == PAPER;
        bool p2scissors = p2choice == SCISSORS;
        bool p2error = !p2rock&&!p2paper&&!p2scissors;

        //Tally Rock, Paper, Scissors counts
        if(p1rock){
            p1.ChoseRock();
        }else if(p1paper){
            p1.ChosePaper();
        }else if(p1scissors){
            p1.ChoseScissors();
        }

        if(p2rock){
            p2.ChoseRock();
        }else if(p2paper){
            p2.ChosePaper();
        }else if(p2scissors){
            p2.ChoseScissors();
        }


        //Handle cases where one or both players make a mistake
        if(p1error && p2error){
            Console.WriteLine("Oops, both players made a mistake... no points for anyone");
        }else if(p1error){
            Console.WriteLine($"Oops, {p1.Name} made a mistake, 6 points for {p2.Name}");
            p2.AddPoints(6);
                
        }else if(p2error){
            Console.WriteLine($"Oops, {p2.Name} made a mistake, 6 points for {p1.Name}");
            p1.AddPoints(6);

        //In case of tie
        }else if(p1choice == p2choice){
            Console.WriteLine($"Both players get 1 point");
            p1.AddPoints(1);
            p2.AddPoints(1);

        //Standard p1 victory cases
        }else if(p1rock&&p2scissors){
            Console.WriteLine($"{p1.Name} gets 2 points!");
            p1.AddPoints(2);
        }else if(p1paper&&p2rock){
            Console.WriteLine($"{p1.Name} gets 4 points!");
            p1.AddPoints(4);
        }else if(p1scissors&&p2paper){
            Console.WriteLine($"{p1.Name} gets 6 points!");
            p1.AddPoints(6);
        
        //Standard p2 victory cases
        }else if(p2rock&&p1scissors){
            Console.WriteLine($"{p2.Name} gets 2 points!");
            p2.AddPoints(2);
        }else if(p2paper&&p1rock){
            Console.WriteLine($"{p2.Name} gets 4 points!");
            p2.AddPoints(4);
        }else if(p2scissors&&p1paper){
            Console.WriteLine($"{p2.Name} gets 6 points!");
            p2.AddPoints(6);
        
        }

    }

    public void runRound(){
        for (int i = 0; i<players.Length/2;i++) {
            Player p1 = players[i];
            Player p2 = players[players.Length-(i+1)];

            Console.WriteLine($"{p2.Name} ({p2.Points} Points) vs {p1.Name} ({p1.Points} Points)");
            //Run 3 games of rock paper scissors for each matchup.
            for (int j = 1; j<4; j++){
                Console.WriteLine($"\nGame {j}:");
                runRPSGame(p1,p2);
                Console.ReadKey();
            }
            Console.WriteLine($"\n{p2.Name} now has {p2.Points} Points, {p1.Name} now has {p1.Points} Points\n");
        }
    }

    public void runRoundRobin(){
        buildTeams();
        for (int i = 0; i< (players.Length - 1); i++){
            Console.WriteLine($"\nStart of Round {i+1}");
            runRound();
            roundRobinShift();
            showStats();
         }
         presentResults();
    }

    private void roundRobinShift(){
        Player swap = players[players.Length - 1];
        for (int i = 1; i < (players.Length); i++){
            Player current = players[i];
            players[i] = swap;
            swap = current;
        }
    }

    private void showStats(){
        Console.WriteLine("\nAfter round, scores are now: \n");
        for (int i = 0; i < players.Length; i++) {
            Console.WriteLine($"{players[i].Name}: {players[i].Points} points");
        }
        Console.ReadKey();
    }

    private void presentResults(){
        Player MostRocks = players[0];   
        Player MostPapers = players[0];
        Player MostScissors = players[0];
        Player MostPoints = players[0];
        
        for (int i = 1; i < (players.Length); i++){
            Player c = players[i];
            if (c.Points > MostPoints.Points){
                MostPoints = c;
            }
            if (c.RockCount > MostRocks.RockCount){
                MostRocks = c;
            }
            if (c.ScissorsCount > MostScissors.ScissorsCount){
                MostScissors = c;
            }
            if (c.PaperCount > MostPapers.PaperCount){
                MostPapers = c;
            }
        }

        Console.WriteLine("Comptetition Over!");
        Console.WriteLine("Results:");
        Console.ReadKey();
        Console.WriteLine($"{MostRocks.Name} threw the most rocks with {MostRocks.RockCount}!");
        Console.ReadKey();
        Console.WriteLine($"{MostPapers.Name} threw the most papers with {MostPapers.PaperCount}!");
        Console.ReadKey();
        Console.WriteLine($"{MostScissors.Name} threw the most scissors with {MostScissors.ScissorsCount}!");
        Console.ReadKey();
        Console.WriteLine($"{MostPoints.Name} won the competition with {MostPoints.Points}!");
        Console.ReadKey();
        Console.WriteLine("Congratulations to everyone for participating!");        
    }

}


//Implementation of the IRPSAi, choses randomly between rock paper and scissors.
class RPSRandomAi : IRPSAi{

    public string GetChoice(Player You, Player Opponent){
        Random rand = new Random();
        int choice = rand.Next(3);
        if (choice == 0){
            return "rock";
        }else if(choice == 1){
            return "paper";
        }else if(choice == 2){
            return "scissors";
        }else{
            return "oops";
        }
    }

}
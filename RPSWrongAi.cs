
//Implementation of the IRPSAi, the randomly throws exceptions or gives an invalid answer.
class RPSWrongAi : IRPSAi{

    public string GetChoice(Player You, Player Opponent){
        Random rand = new Random();
        int choice = rand.Next(3);
        if (choice == 0){
            throw new Exception("huh?");
        }else if(choice == 1){
            return "what?";
        }else{
            return null;
        }
    }
        
}

